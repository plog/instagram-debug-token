import express from 'express';
import request from 'request';

let router = express.Router();

const clientId = '7e911b7430504e47ab90b13e194fc148';
const redirectUrl = 'http://lvh.me:3333';

const url = `https://instagram.com/oauth/authorize/?client_id=${clientId}&redirect_uri=${redirectUrl}&response_type=token`;

router.get('/', (req, res) => {
  res.render('index', { title: 'Hello' });
});

router.get('/auth', (req, res) => {
  res.redirect(url);
});

router.post('/login', (req, res, next) => {
  let token = req.body.token;
  let cb = (err, response) => {
    if (err) {
      console.log(err);
      return next(err);
    }
    res.json(response);
  };
  request({
    uri: '/api/2.0/auth/token',
    baseUrl: 'http://lvh.me:3000',
    body: { token: token },
    method: 'POST',
    json: true
  }, cb);
});

export default router;
