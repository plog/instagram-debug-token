import crypto from 'crypto';

import request from 'request';

const baseUrl = 'https://api.instagram.com/v1/';

export default class InstagramClient {
  constructor(clientId, clientSecret) {
    if (!clientId || !clientSecret) {
      throw new Error('InstagramClient: Missing `clientId` or `clientSecret`.');
    }

    this.clientId = clientId;
    this.clientSecret = clientSecret;
  }

  /**
   * Make a request to the instagram api
   * @param {String} method http method to use.
   * @param {string} endpoint Endpoint to call.
   * @param {object} opts Options. Use `params` to set the data to send and the
   * access_token. Set `sign` to `true` if you want the request to be signed.
   * @param {function} cb The request callback.
   */
  request(method, endpoint, opts, cb) {
    let sig;
    if (opts.sign) {
      sig = this.signRequest(endpoint, opts.params);
    }

    let params = opts.params;
    if (sig) {
      params.sig = sig;
    }

    request({
      uri: endpoint,
      baseUrl: baseUrl,
      qs: params,
      method: method,
      json: true
    }, cb);
  }

  /**
   * Make a GET request to the instagram api
   * @param {string} endpoint Endpoint to call.
   * @param {object} opts Options. Use `params` to set the data to send and the
   * access_token. Set `sign` to `true` if you want the request to be signed.
   * @param {function} cb The request callback.
   */
  get(endpoint, opts, cb) {
    this.request('GET', endpoint, opts, cb);
  }

  /**
   * Computes a request signature.
   * @param {string} endpoint Endpoint to call.
   * @param {object} params Request params.
   * @return {undefined}
   */
  signRequest(endpoint, params) {
    params = params || {};
    let sig = endpoint;
    let sortedKeys = Object.keys(params).sort();

    for (let key of sortedKeys) {
      sig += `|${key}=${params[key]}`;
    }

    let hmac = crypto.createHmac('sha256', this.clientSecret);
    hmac.update(sig);
    return hmac.digest('hex');
  }
}
